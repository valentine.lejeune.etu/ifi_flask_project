![FLASK](./readme_img/Flask.png)


Rezozio. Le retour
===

Ceux d'entre vous qui etaient present en L2, gardent sûrement le douloureux souvenir de ce projet de TW2.  
Nous avions comme projet, de réaliser un super réseau social totalement innovant (et pas du tout inspiré de twitter)
 à l'aide de nos maigres connaissances en web. Jusqu'ici tout vas bien, à ceci près que ce dernier devait etre developé
  en php ...  

![PHP1](./readme_img/my_first_project_with_php.jpeg)  
---  

Il est maintenant temps de prendre notre revanche et de le redévelopper, mais cette fois-ci, de manière très simple
 et très rapide !

## Installer flask

Cette simple commande vous permettra d'installer Flask !
```sh
pip install Flask
```

## Saluons le monde !

Commencez par créer un fichier python appelé `app.py`.

```python
from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

if __name__ == "__main__":
    app.run()
```

Enfin executez le script !

```sh
python app.py
```
Et  vous verrez un magnifique "Hello World" s'afficher sur votre page si vous vous rendez à l'adresse
 http://localhost:5000/  

Voila, sur ce, merci à vous pour ce TP, et bonne soirée !

Non ? Bon alors continuons !  

## De l'html ! youpi !

 Tout ça c'est bien beau mais ça manque un peu d'html non ?  
Modifiez legerement la valeur de retour de votre fonction `hello` afin de retourner des balises html, 
par exemple des balises `<h1>`.  

Pour que la modification soit repercutée sur le serveur il suffit de kill le serveur, de le relancer et de refresh 
la page, pratique non ? ... Comment ça non ?
Bon très bien, si vous voulez que le serveur se rafraichisse après chaque sauvegarde du code, vous n'avez qu'à
 activer le mode **DEBUG**. Une fois le mode **DEBUG** activé, un simple rafraichissement de la page web suffira.   
Pour ce faire, il vous suffit de rajouter `debug=True` dans le lancement de votre appli :

  ```python
    app.run(debug=True)
  ```  

Très bien, maintenant que c'est fait, vous vous dites bien evidemment que cela est très bien, que Flask 
est vraiment un Framework genial mais que mettre de l'html dans le code python c'est vraiment pas fou ! 
Et vous avez raison, c'est pourquoi Flask intègre jinja2, un moteur de template html plutôt puissant et 
pratique à utiliser ( [voir la doc](https://jinja.palletsprojects.com/) ).

Pour utiliser ces fameux templates, la première chose à faire est de creer un dossier nommé `templates` 
( pourquoi être original après tout ) à la racine de votre projet, au meme niveau que votre `app.py` donc.

Dans ce dossier `templates`, creez votre premier template, `home.html`.

Ensuite, de retour dans votre `app.py`, remplacez votre hideuse chaine de caractre html par une magnifique
 invocation de votre template.  
( N'oubliez pas d'importer `render_template` depuis le module flask )

  ```python
    render_template('home.html')
  ```  

## Plus de routes !

C'est le moment de faire grandir votre application en lui ajoutant plus de routes !

Avant d'aller plus loin, afin de faire fonctionner l'appli correctement, il est necessaire d'ajouter une
 clé secrete à notre application. Elle nous servira à encoder un tas de chose pour des raisons de securité.
 Elle sera aussi utilisée de façon interne par flask, il faut donc la specifier :
 ```python
app.config['SECRET_KEY'] = 'YOUR_RANDOM_STRING'
```

 Une fois cela fait, nous pouvons continuer
 
 D'abord, renommez votre method `hello` en `home` (pour plus de coherence) et associez lui, en plus de `/`, la route `/home`

Ensuite, ajoutez à votre application une page `About` ( avec le template html associé ) qui contiendra (ou pas)
certaines informations à propos des développeurs du nouveau reseau social à la mode.
Vous êtes très doués et nous sommes sûrs que vous y arriverez seuls.


Si vous avez tout suivi jusqu'a maintenant, vous devriez avoir vos deux pages `home.html` et `about.html`,
 qui ne contiennent pas grand chose.

Essayons d'etoffer un peu nos pages html et d'y inclure un peu de style.

Dans notre infinie bonté, nous allons vous donner un magnifique template, incluant un header qui vous simplifiera
 quelque peu la vie :
Remplacez donc votre le contenu de votre `home.html` par le celui du fichier `template.html` present dans le 
dossier `snippets`( en n'oubliant pas de le personaliser )

Ensuite, ajoutons un peu de css.  
Creez un nouveau dossier `static` contenant un fichier `main.css`.
Et parce que le css c'est trop cool vous allez devoir tout faire vous meme ! (ou alors, allez le chercher dans le 
dossier `snippets`)

Ensuite, liez cette super feuille de style à votre template  ( tips : utilisez la fonction `url_for` )

Voila, votre page principale ressemble enfin à quelque chose ! Maintenant, visitez votre page `about`

Mince, le header et le style ont disparus !
Tout simplement car vos deux pages html sont differentes. La solution la plus rapide serait de copier-coller 
l'html dans tout vos templates. 
Mais si vous etiez attentifs lors de tous ces cours qui vous ont été prodigués avec soin par notre corps enseignant 
hyper qualifié, alors vous savez que ce n'est pas une bonne chose.

La solution est donc d'exploiter la mécanique d'heritage !

Je vous laisse donc creer un template parent `layout.html` dans lequel vous placerez le code html precedent.

Ensuite vous le surchargerez dans vos templates `home.html` et `about.html`.


## Ajoutons les posts !
Que serait notre cher  Rezozio sans ses messages !

```python
dummyDatas = [
  {
      "author": "Bob",
      "title": "First Post",
      "content": "Awesome content",
      "date": "November 24, 2019"
  },
  {
      "author": "John",
      "title": "Second Post",
      "content": "Awesome content",
      "date": "November 25, 2019"
  },
  {
      "author": "Eugene",
      "title": "Third Post",
      "content": "Awesome content",
      "date": "November 26, 2019"
  }
]
```

Ajoutons donc ces dummyDatas a notre appli. Mettez les simplement dans votre `app.py` et passez les en paramètre à
 votre template `home.html` dans lequel vous ferez une super boucle pour afficher tous vos articles !
  (en vous aidant du snippet `article.html`).

Maintenant que vous avez votre liste d'articles affichés. Passons a la suite !

## Des formulaires !!! Oh yeah !

Nous aimerions maintenant nous pouvoir nous connecter. 
Pour cela, nous devons créer deux nouvelles page, `register.html` et `login.html` ( qui héritent elles aussi 
de `layout.html`) et les remplir avec des formulaires.
Pour vous faire gagner un peu de temps, nous vous avons mis à disposition un formulaire présent dans 
le dossier `snippets`.  

Et la vous vous dites qu'en faite, Flask, ce n'est ni plus ni moins que du html ... Jusqu'à maintenant c'est 
presque vrai, alors essayons de gerer ces fameux formulaires en python !  

Pour manipuler ces formulaires, nous allons utiliser une petite librairie tout droit sorti de derriere les fagots :
 `flask_wtf` ! (c'est son vrai nom, je vous assure )  

Avant de pouvoir commencer, il faut donc importer ladite librairie :

```bash
pip install flask_wtf
```

Ensuite créez un nouveau fichier python : `forms.py`, dans lequel vous écrirez votre premier formulaire Flask !
Pour le formulaire d'enregistrement, vous devrez mettre en place plusieurs validation :   
<ul>
    <li>Tout les champs doivent être requis  </li>
    <li>L'adresse mail doit être une adresse mail valide</li>
    <li>Le login doit faire entre 2 et 20 caracteres  </li>
    <li>La confirmation du mot de mot doit evidemment être égale au mot de passe.  </li>
</ul>
Pour le formulaire de connexion :  
<ul>
    <li>Tous les champs doivent être requis  </li>
    <li>L'adresse mail doit etre une adresse mail valide</li>
</ul>
       
Une fois vos magnifiques classes de formulaires créées, vous devrez les instancier dans les routes concernées,
 et les mettre en page dans vos templates html associés. Pour ce faire, vous n'avez qu'a modifier votre formulaire html 
 pour y inclure votre formulaire python (cf présentation).  

Parfait, maintenant vous avez vos formulaires, avec validation de champ. Cependant si vous soumettez le formulaire,
vous recevez un magnifique `Method Not Allowed`  

Et oui, car comme nous l'avons dit precedement lors de notre presentation, les routes sont definies par defaut pour 
la méthode GET. Vous devez donc rajouter la methodes POST aux methodes autorisées.  

Une fois cette modification effectuée, vous vous rendrez compte que vous ne recevez aucun feedback de la part de votre 
formulaire. Ce qui est bien dommage !  

Pour corriger le tir, vous devrez faire trois choses :  
<ul>
    <li>La première, déclencher la validation du formulaire ( et des ses inputs )  </li>
    <li>La deuxième, modifier l'affichage du formulaire en fonction des erreurs reçues  
    ( si l'addresse mail est invalide par exemple )  </li>
    <li>La troisième, notifier l'utilisateur (en utilisant flash) lorsqu'il est correctement authentifié 
    , puis le rediriger vers la page d'acceuil </li>
</ul>

## Mesdames et messieurs, faites place à la BDD !

Il est temps de cesser d'utiliser ces dummyDatas. Nous allons donc mettre un place une petite base de données grâce
à SQLAlchemy ! (Et plus particulierement sa version adaptée à Flask donc voici 
[la doc](https://flask-sqlalchemy.palletsprojects.com/en/2.x/))

Comme d'habitude, il est necessaire d'installer `flask-sqlalchemy` via pip.

Ensuite, pour set up la bdd, rien de plus simple :
Ajoutez cette ligne afin de specifier l'uri permettant d'acceder à la BDD
```python
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'
```
<ul>
    <li>`sqlite` permet d'indiquer que la bdd sera en fait set up a partir d'un fichier.</li>
    <li>`site.db` est le nom du fichier qui sera créé pour la db (le triple / sert a indiquer un path relatif,
     c'est à dire que le fichier sera créé à la racine du projet)</li>
</ul>
Puis, créez une instance de la base de données grâce à :  

```python
from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy(app)
```
Ensuite, vous devez créer vos tables. Pour se faire, rien de plus simple, il suffit de definir de nouvelles classes 
heritant de `db.Model` !

Creez donc la classe `Post`, qui contiendras les mêmes champs que nos dummyDatas.
Rajoutez y un id en clé primaire et faites en sorte que la date soit generée par defaut ( utilisez la fonction `utcnow` du module
`datetime`)

Créez ensuite un formulaire, et la route associée pour ajouter un post à notre blog !
 (pour la premiere version, vous devrez specifier l'auteur de chaque poste )  
N'oubliez pas de créer les tables comme nous vous avons montré lors de la présentation !  
Si tout c'est bien passé, vous devriez déjà pouvoir ajouter et consulter des posts !!!!


## Pour aller plus loin ...

Si vous êtes arrivés jusque là, c'est que vous êtes maintenant un expert en Flask 
(ou alors que Flask c'est hyper facile et mega rapide ...)

Votre mission est donc d'implémenter la gestion des utilisateurs. 

Vous devrez donc 
<ul>
    <li> Créer la table User et la lier a vos formulaire d'enregistrement et de login</li>
    <li> Gerer les sessions utilisateurs </li>
    <li> Lier les tables Post et User  </li>
</ul>

(pour les sessions utilisateurs => voir [flask_login](https://flask-login.readthedocs.io/en/latest/) (à installer via pip ))
  
## Pour aller ENCORE plus loin ...
Bon, cette fois, si vous êtes arrivés jusque là, c'est que vous êtes VRAIMENT un expert en Flask 
(ou alors qu'on a mal dosé la durée du TP ...)

Enfin bref, pour terminer essayez d'ajouter la possibilité de modifier et de supprimer un POST
 (seulement si l'utilisateur connecté en est l'auteur ...)
